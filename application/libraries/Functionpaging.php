<?php
Class Functionpaging {
  public function paging($item_per_page, $current_page, $total_records, $total_pages)
  {
      $pagination = '';
      if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
          $pagination .= '<ul class="pagination page-numbers">';

          $right_links    = $current_page + 3;
          $previous       = $current_page - 2; //previous link
          $next           = $current_page + 1; //next link
          $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
        $previous_link = ($previous==0)? 1: $previous;
              $pagination .= '<li><a href="1" data-page="1" title="First"><<</a></li>'; //first link
              $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous" class="previous page-numbers"><</a></li>'; //previous link
                  for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                      if($i > 0){
                          $pagination .= '<li><a href="daftar/'.$i.'" data-page="'.$i.'" title="Page'.$i.'"  class="page-numbers">'.$i.'</a></li>';
                      }
                  }
              $first_link = false; //set first link to false
          }

          if($first_link){ //if current active page is first link
              $pagination .= '<li class="active"><a href="javascript:;" style="color: #fff;"><span class="page-numbers current">'.$current_page.'</span></a></li>';
          }elseif($current_page == $total_pages){ //if it's the last active link
              $pagination .= '<li class="active"><a href="javascript:;" style="color: #fff;"><span class="page-numbers current">'.$current_page.'</span></a></li>';
          }else{ //regular current link
              $pagination .= '<li class="active"><a href="javascript:;" style="color: #fff;"><span class="page-numbers current">'.$current_page.'</span></a></li>';
          }

          for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
              if($i<=$total_pages){
                  $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'"  class="page-numbers">'.$i.'</a></li>';
              }
          }
          if($current_page < $total_pages){
          $next_link = ($i > $total_pages) ? $total_pages : $i;
                  $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next" class="next page-numbers">></a></li>'; //next link
                  $pagination .= '<li><a href="#" data-page="'.$total_pages.'" title="Last">>></a></li>'; //last link
          }

          $pagination .= '</ul>';
      }
      return $pagination; //return pagination links
  }
}
?>
