<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_admin($where = null, $order_by = null)
	{
		$this->db->select();
		$this->db->from('tbl_admin');
		if ($where != null) {
			$this->db->where($where);
		}
		if ($order_by != null) {
			$this->db->order_by($order_by);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
}
