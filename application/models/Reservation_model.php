<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reservation_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function save_reservation($data)
	{
		return $this->db->insert('tbl_reservation', $data);
	}
}
