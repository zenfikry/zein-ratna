<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guest_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function save_guest($data)
	{
		return $this->db->insert('tbl_guest', $data);
	}

	public function update_guest($data, $where)
	{
		$this->db->where($where);
		return $this->db->update('tbl_guest', $data);
	}

	public function delete_guest($where)
	{
		return $this->db->delete('tbl_guest', $where);
	}

	public function get_guest($where = null, $order_by = null)
	{
		$this->db->select();
		$this->db->from('tbl_guest');
		if ($where != null) {
			$this->db->where($where);
		}
		if ($order_by != null) {
			$this->db->order_by($order_by);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function guest_num_rows($search_by, $keywords)
	{
		$where = "WHERE id_guest != ''";
		if ($keywords != "") {
			$keywords = explode(' ', $keywords);
			foreach ($keywords as $keyword) {
				$where .= " AND " . $search_by . " LIKE '%" . trim($keyword) . "%'";
			}
		}

		$query = $this->db->query("SELECT * FROM tbl_guest $where");
		return $query->num_rows();
	}

	public function list_guest($page_position, $per_page, $sort_by, $order_by, $search_by, $keywords)
	{
		$where = "WHERE id_guest != ''";
		if ($keywords != "") {
			$keywords = explode(' ', $keywords);
			foreach ($keywords as $keyword) {
				$where .= " AND " . $search_by . " LIKE '%" . trim($keyword) . "%'";
			}
		}

		$query = $this->db->query("SELECT * FROM tbl_guest $where ORDER BY $sort_by $order_by LIMIT $page_position, $per_page");
		return $query->result_array();
	}
}
