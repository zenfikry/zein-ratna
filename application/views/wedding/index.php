﻿<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Zein & Ratna</title>
	<!-- Favicon -->
	<link rel="icon" href="<?php echo base_url() ?>assets-web/images/favicon.ico" type="image/x-icon">

	<!-- Library CSS -->
	<link href="<?php echo base_url() ?>assets-web/css/glanz_library.css" rel="stylesheet">

	<!-- Icons CSS -->
	<link href="<?php echo base_url() ?>assets-web/fonts/themify-icons.css" rel="stylesheet">

	<!-- Theme CSS -->
	<link href="<?php echo base_url() ?>assets-web/css/glanz_style.css" rel="stylesheet">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Dosis:300,400,600,700%7COpen+Sans:300,400,700%7CPlayfair+Display:400,400i,700,700i" rel="stylesheet">

	<!-- Other Fonts -->
	<link href="<?php echo base_url() ?>assets-web/fonts/marsha/stylesheet.css" rel="stylesheet">

	<!-- Custom style -->
	<link href="<?php echo base_url() ?>assets-web/css/custom-style.css" rel="stylesheet">
</head>

<body class="gla_middle_titles">
	<input type="hidden" id="base_url" value="<?php echo base_url() ?>">
	<div class="page-loader-wrapper" id="invitation_popup">
		<!-- <div class="gla_invitation_container"> -->
		<div class="gla_invitation_i gla_invitation_ii gla_image_bck" data-image="<?php echo base_url() ?>assets-web/images/invitations/inv_i/back4.jpg" style="padding: 80px 120px;">
			<p><img src="<?php echo base_url() ?>assets-web/images/invitations/inv_i/save_the_date_bl.gif" alt="" height="240"></p>
			<br><br>
			<h2 style="margin-bottom: 40px;">Ratna & Zein</h2>
			<h5>Special invite to:</h5>
			<h4><?php echo $name_guest ?></h4>
			<button type="button" class="btn" onclick="$('#invitation_popup').hide()" style="margin-top: 10px;">Open Invitation</button>
		</div>
		<!-- </div> -->
	</div>

	<!-- Page -->
	<div class="gla_page" id="gla_page">
		<!-- To Top -->
		<a href="#gla_page" class="gla_top ti ti-angle-up gla_go"></a>

		<!-- Slider -->
		<div class="gla_slider gla_image_bck  gla_wht_txt gla_fixed" data-image="<?php echo base_url() ?>assets-web/images/photo/DSC0032.jpg" data-stellar-background-ratio="0.2">
			<!-- Over -->
			<div class="gla_over" data-color="#000" data-opacity="0.5"></div>
			<div class="container">
				<!-- Slider Texts -->
				<div class="gla_slide_txt gla_slide_center_middle text-center">
					<p><img src="<?php echo base_url() ?>assets-web/images/animations/ourwedding_gold.gif" alt="" height="330"></p>
					<div class="gla_slide_subtitle">Ratna & Zein <br>13 Juni 2021</div>
				</div>
				<!-- Slider Texts End -->
			</div>
			<!-- container end -->

			<!-- Slide Down -->
			<a class="gla_scroll_down gla_go" href="#gla_content">
				<b></b>
				Scroll
			</a>
		</div>
		<!-- Slider End -->

		<!-- Content -->
		<section id="gla_content" class="gla_content">
			<!-- section -->
			<section class="gla_section gla_image_bck" data-color="#">
				<div class="container text-center">
					<p><img src="<?php echo base_url() ?>assets-web/images/animations/flowers4.gif" height="200" alt=""></p>
					<p>"Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu pasangan hidup dari jenismu sendiri, supaya kamu mendapat ketenangan hati, dan dijadikan-Nya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berfikir." — Ar-Rum:21</p>
				</div>
				<!-- container end -->
			</section>
			<!-- section end -->

			<!-- section -->
			<section class="gla_section gla_fixed" style="background-color: #f7f7f7;">
				<div class="container">
					<div class="row gla_auto_height">
						<div class="col-md-6 gla_image_bck" data-color="#eee">
							<div class="gla_simple_block">
								<h2>Ratna Permatafuri, S.Pd.</h2>
								<b>Putri kedua dari:</b>
								<p style="margin-bottom: 0px;">Bapak Sumarno, S.Pd</p>
								<p style="margin-bottom: 10px;">& Ibu Dede Suryani</p>
								<!-- Social Buttons -->
								<div class="gla_footer_social">
									<a href="https://www.instagram.com/ratnapermatafuri/" target="_blank"><i class="ti ti-instagram gla_icon_box"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-6 gla_image_bck" data-image="<?php echo base_url() ?>assets-web/images/photo/DSC0019.jpg">

						</div>
						<div class="col-md-6 col-md-push-6 gla_image_bck" data-color="#eee">
							<div class="gla_simple_block">
								<h2>Muhammad Zein Fikry Lazuadi, S.T.</h2>
								<b>Putra pertama dari:</b>
								<p style="margin-bottom: 0px;">Bapak Drs. H. Aji Surya Suraji, M.Pd.</p>
								<p style="margin-bottom: 10px;">& Ibu Dra. Hj. Maryati, M.Pd.I.</p>
								<!-- Social Buttons -->
								<div class="gla_footer_social">
									<a href="https://www.instagram.com/kang.jae.in/" target="_blank"><i class="ti ti-instagram gla_icon_box"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-md-pull-6 gla_image_bck" data-image="<?php echo base_url() ?>assets-web/images/photo/DSC0021.jpg">

						</div>
					</div>
				</div>
				<!-- container end -->
			</section>
			<!-- section end -->

			<!-- section -->
			<section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="<?php echo base_url() ?>assets-web/images/photo/IMG_9626_copy.jpg">
				<!-- Over -->
				<div class="gla_over" data-color="#000" data-opacity="0.7"></div>

				<div class="container text-center">
					<p><img src="<?php echo base_url() ?>assets-web/images/animations/save_gold.gif" height="280" alt=""></p>
					<h2>13 Juni 2021</h2>
					<h3>Kp. Cireundeu Ds. Cipaingeun Kec. Sodonghilir,<br>Kab. Tasikmalaya</h3>
					<div class="gla_countdown" data-year="2021" data-month="06" data-day="13"></div>
				</div>
				<!-- container end -->
			</section>
			<!-- section end -->

			<!-- section -->
			<section class="gla_section">
				<div class="container text-center" style="padding-top: 30px;">
					<div class="row text-center">
						<div class="col-md-4 col-md-push-2 gla_round_block">
							<div class="gla_round_im gla_image_bck" data-image="<?php echo base_url() ?>assets-web/images/wedding/ian_kelsey/600x600/14818599945_45817b7c25_k.jpg" style="display: none;"></div>
							<h3 style="margin-bottom: 20px;">Akad Nikah</h3>
							<p>13 Juni 2021<br>
								09.00 WIB<br>
								Kp. Cireundeu Ds. Cipaingeun Kec. Sodonghilir,<br>
								Kab. Tasikmalaya</p>
							<a href="https://goo.gl/maps/31ZDg3cQLBP8TUXD9" class="btn" target="_blank">View Map</a>
						</div>
						<div class="col-md-4 col-md-push-2 gla_round_block">
							<div class="gla_round_im gla_image_bck" data-image="<?php echo base_url() ?>assets-web/images/wedding/ian_kelsey/600x600/14814281111_b4f0b02491_k.jpg" style="display: none;"></div>
							<h3 style="margin-bottom: 20px;">Resepsi</h3>
							<p>13 Juni 2021<br>
								11.00 - 14.00 WIB<br>
								Kp. Cireundeu Ds. Cipaingeun Kec. Sodonghilir,<br>
								Kab. Tasikmalaya</p>
							<a href="https://goo.gl/maps/31ZDg3cQLBP8TUXD9" class="btn" target="_blank">View Map</a>
						</div>
					</div>
				</div>
				<!-- container end -->
			</section>
			<!-- section end -->

			<!-- section -->
			<section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="<?php echo base_url() ?>assets-web/images/photo/IMG_9626_copy.jpg">
				<!-- Over -->
				<div class="gla_over" data-color="#000" data-opacity="0.7"></div>

				<div class="container text-center">
					<h2>Gallery</h2>

					<!-- grid -->
					<div class="gla_portfolio_no_padding grid">
						<div class="col-sm-4 col-xs-6 gla_anim_box grid-item engagement">
							<div class="gla_shop_item">
								<a href="<?php echo base_url() ?>assets-web/images/photo/DSC0010 copy.jpg" class="lightbox">
									<img src="<?php echo base_url() ?>assets-web/images/photo/DSC0010 copy.jpg" alt="">
								</a>
							</div>
						</div>

						<div class="col-sm-4 col-xs-6 gla_anim_box grid-item ceremony">
							<div class="gla_shop_item">
								<a href="<?php echo base_url() ?>assets-web/images/photo/DSC0011 copy.jpg" class="lightbox">
									<img src="<?php echo base_url() ?>assets-web/images/photo/DSC0011 copy.jpg" alt="">
								</a>
							</div>
						</div>

						<div class="col-sm-4 col-xs-6 gla_anim_box grid-item engagement">
							<div class="gla_shop_item">
								<a href="<?php echo base_url() ?>assets-web/images/photo/DSC0029 copy.jpg" class="lightbox">
									<img src="<?php echo base_url() ?>assets-web/images/photo/DSC0029 copy.jpg" alt="">
								</a>
							</div>
						</div>

						<div class="col-sm-4 col-xs-6 gla_anim_box grid-item ceremony">
							<div class="gla_shop_item">
								<a href="<?php echo base_url() ?>assets-web/images/photo/DSC0064 copy.jpg" class="lightbox">
									<img src="<?php echo base_url() ?>assets-web/images/photo/DSC0064 copy.jpg" alt="">
								</a>
							</div>
						</div>

						<div class="col-sm-4 col-xs-6 gla_anim_box grid-item engagement">
							<div class="gla_shop_item">
								<a href="<?php echo base_url() ?>assets-web/images/photo/DSC0007 copy.jpg" class="lightbox">
									<img src="<?php echo base_url() ?>assets-web/images/photo/DSC0007 copy.jpg" alt="">
								</a>
							</div>
						</div>

						<div class="col-sm-4 col-xs-6 gla_anim_box grid-item engagement">
							<div class="gla_shop_item">
								<a href="<?php echo base_url() ?>assets-web/images/photo/DSC0071 copy.jpg" class="lightbox">
									<img src="<?php echo base_url() ?>assets-web/images/photo/DSC0071 copy.jpg" alt="">
								</a>
							</div>
						</div>
					</div>
					<!-- grid end -->
				</div>
				<!-- container end -->
			</section>
			<!-- section end -->

			<!-- section -->
			<section class="gla_section gla_fixed" style="background-color: #f7f7f7;">

				<div class="container text-center">
					<p><img src="<?php echo base_url() ?>assets-web/images/animations/rsvp_gold_wh.gif" height="200" alt=""></p>
					<div class="row">
						<div class="col-md-6 col-md-push-3">
							<form id="form_reservation">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Nama*</label>
											<input type="text" name="name" class="form-control form-opacity" required>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<label>Alamat*</label>
											<input type="text" name="address" class="form-control form-opacity" required>
										</div>
									</div>

									<div class="col-md-12">
										<label>Konfirmasi kehadiran?</label>

										<input type="radio" name="attend" value="Saya akan hadir" checked>
										<span>Saya akan hadir</span><br>
										<input type="radio" name="attend" value="Saya masih ragu">
										<span>Saya masih ragu</span><br>
										<input type="radio" name="attend" value="Saya tidak bisa hadir">
										<span>Saya tidak bisa hadir</span>
									</div>

									<div class="col-md-12" style="margin-top: 20px;">
										<input type="submit" class="btn submit" value="Kirim">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- container end -->
			</section>
			<!-- section end -->

			<section class="gla_section">
				<div class="container text-center" style="padding-top: 70px;">
					<h3>Kirim Ucapan</h3>
					<div class="row text-center">
						<div class="col-md-6 col-md-push-3">
							<form id="form_wedding_wishes">
								<div class="row">
									<div class="col-md-12">
										<label>Nama*</label>
										<input type="text" name="name" class="form-control" required>
									</div>

									<div class="col-md-12">
										<label>Pesan & doa*</label>
										<textarea name="message" class="form-control" required></textarea>
									</div>

									<div class="col-md-12">
										<button type="submit" class="btn submit" id="btn_send_wishes">Kirim</button>
									</div>
								</div>
							</form>
						</div>
					</div>

					<div class="row" style="margin-top: 30px; height: 200px; overflow-y: auto;">
						<div class="col-md-12">
							<div class="cart">
								<div class="cart-table" id="list_wedding_wishes">

								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- section -->
			<section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="<?php echo base_url() ?>assets-web/images/wedding/ian_kelsey/14812748891_aef5b27a4a_k.jpg">
				<!-- Over -->
				<div class="gla_over" data-color="#000" data-opacity="0.7"></div>

				<div class="container text-center">
					<p><img src="<?php echo base_url() ?>assets-web/images/animations/thanks_gold.gif" height="143" alt=""></p>
				</div>
				<!-- container end -->
			</section>
			<!-- section end -->
		</section>
		<!-- Content End -->
	</div>
	<!-- Page End -->

	<!-- JQuery -->
	<script src="<?php echo base_url() ?>assets-web/js/jquery-1.12.4.min.js"></script>
	<!-- Library JS -->
	<script src="<?php echo base_url() ?>assets-web/js/glanz_library.js"></script>

	<!-- Theme JS -->
	<script src="<?php echo base_url() ?>assets-web/js/glanz_script.js"></script>

	<!-- Jquery Validation Plugin Js -->
	<script src="<?php echo base_url('assets-admin') ?>/plugins/jquery-validation/jquery.validate.js"></script>
	<script src="<?php echo base_url('assets-admin') ?>/plugins/jquery-validation/localization/messages_id.js"></script>

	<!-- Custom JS -->
	<script src="<?php echo base_url() ?>assets-web/js/custom.js?1.0.0"></script>

	<!-- Sweetalert -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script>
		$(document).ready(function() {
			loadWeddingWishes();
		});
	</script>
</body>

</html>