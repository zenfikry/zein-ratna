<!DOCTYPE html>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<title>Webadmin</title>
	<!-- Favicon-->
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url('assets-admin') ?>/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Waves Effect Css -->
	<link href="<?php echo base_url('assets-admin') ?>/plugins/node-waves/waves.css" rel="stylesheet" />

	<!-- Animation Css -->
	<link href="<?php echo base_url('assets-admin') ?>/plugins/animate-css/animate.css" rel="stylesheet" />

	<!-- Custom Css -->
	<link href="<?php echo base_url('assets-admin') ?>/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url('assets-admin') ?>/css/custom-style.css?1.0.0" rel="stylesheet">

	<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
	<link href="<?php echo base_url('assets-admin') ?>/css/themes/all-themes.css" rel="stylesheet" />

	<!-- Bootstrap Material Datepicker -->
	<link href="<?php echo base_url('assets-admin') ?>/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">

	<!-- Bootstrap Select Css -->
	<link href="<?php echo base_url('assets-admin') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

	<!-- Jquery Core Js -->
	<script src="<?php echo base_url('assets-admin') ?>/plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core Js -->
	<script src="<?php echo base_url('assets-admin') ?>/plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Bootstrap Material Datepicker Js -->
	<script src="<?php echo base_url('assets-admin') ?>/plugins/momentjs/moment.js"></script>
	<script src="<?php echo base_url('assets-admin') ?>/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
</head>
<input type="hidden" id="base_url" value="<?php echo base_url() ?>">

<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper hide" style="opacity:0.5" id="loading">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>
	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>

	<!-- Top Bar -->
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
				<a href="javascript:void(0);" class="bars"></a>
				<a class="navbar-brand" href="<?php echo base_url() ?>assets-admin/index.html">ADMINBSB - MATERIAL DESIGN</a>
			</div>
		</div>
	</nav>

	<section>
		<!-- Left Sidebar -->
		<aside id="leftsidebar" class="sidebar">
			<!-- User Info -->
			<div class="user-info">
				<div class="image">
					<img src="<?php echo base_url() ?>assets-admin/images/user.png" width="48" height="48" alt="User" />
				</div>
				<div class="info-container">
					<div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('name_admin') ?></div>
					<div class="email"><?php echo $this->session->userdata('username_admin') ?></div>
					<div class="btn-group user-helper-dropdown">
						<i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
						<ul class="dropdown-menu pull-right">
							<li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
							<li role="seperator" class="divider"></li>
							<li><a href="<?php echo base_url('webadmin/login/act_logout') ?>"><i class="material-icons">input</i>Log Out</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- #User Info -->

			<!-- Menu -->
			<div class="menu">
				<ul class="list">
					<li class="header">MAIN NAVIGATION</li>
					<li>
						<a href="<?php echo base_url('webadmin') ?>">
							<i class="material-icons">home</i>
							<span>Home</span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('webadmin/guest') ?>">
							<i class="material-icons">group</i>
							<span>Tamu Undangan</span>
						</a>
					</li>
				</ul>
			</div>
			<!-- Footer -->
			<div class="legal">
				<div class="copyright">
					&copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
				</div>
				<div class="version">
					<b>Version: </b> 1.0.5
				</div>
			</div>
			<!-- #Footer -->
		</aside>
	</section>