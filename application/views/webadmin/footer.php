<!-- Slimscroll Plugin Js -->
<script src="<?php echo base_url('assets-admin') ?>/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php echo base_url('assets-admin') ?>/plugins/node-waves/waves.js"></script>

<!-- Input Mask Plugin Js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>

<!-- Custom Js -->
<script src="<?php echo base_url('assets-admin') ?>/js/custom.js"></script>

<!-- Input Mask Plugin Js -->
<script src="<?php echo base_url('assets-admin') ?>/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

<!-- Select Plugin Js -->
<script src="<?php echo base_url('assets-admin') ?>/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Sweetalert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Jquery Validation Plugin Js -->
<script src="<?php echo base_url('assets-admin') ?>/plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets-admin') ?>/plugins/jquery-validation/localization/messages_id.js"></script>

</body>
</html>