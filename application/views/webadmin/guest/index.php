<section class="content">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url('webadmin') ?>">
					<i class="material-icons">home</i> Home
				</a>
			</li>
			<li class="active">
				Tamu Undangan
			</li>
		</ol>

		<div class="row clearfix">
			<!-- Basic Examples -->
			<div class="col-md-12">
				<div class="card">
					<div class="header p-b-0-i">
						<div class="row">
							<div class="col-md-2">
								<button type="button" class="btn btn-primary waves-effect m-t-20" title="Tambah" onclick="addGuest()">
									<i class="material-icons">add</i>
								</button>
							</div>
							<div class="col-md-2">
								<label for="sort_by">Sort By</label>
								<div class="form-group">
									<div class="form-line">
										<select class="form-control" id="sort_by" data-filter="sortBy-guest" onchange="setFilter(this, false)">
											<option value="created_at" selected>Tgl. Input</option>
											<option value="name_guest">Nama</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<label for="order_by">Order By</label>
								<div class="form-group">
									<div class="form-line">
										<select class="form-control" id="order_by" data-filter="orderBy-guest" onchange="setFilter(this, false)">
											<option value="desc" selected>DESC</option>
											<option value="asc">ASC</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<label for="search_by">Search By</label>
								<div class="form-group">
									<div class="form-line">
										<select class="form-control" id="search_by" data-filter="searchBy-guest" onchange="setFilter(this, false)">
											<option value="name_guest" selected>Nama</option>
											<option value="phone_guest">No. Telpon</option>
											<option value="address_guest">Alamat</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<label for="keywords">Search</label>
								<div class="input-group m-b-0-i">
									<div class="form-line">
										<input type="text" class="form-control" placeholder="Search..." id="keywords" data-filter="keywords-guest" onchange="setFilter(this, false)">
									</div>
									<span class="input-group-addon">
										<button type="button" onclick="listGuest(1)" class="btn bg-purple btn-circle waves-effect waves-circle waves-float" title="Cari">
											<i class="material-icons">search</i>
										</button>
										<button type="button" onclick="refresh()" class="btn bg-blue btn-circle waves-effect waves-circle waves-float" title="Refresh">
											<i class="material-icons">refresh</i>
										</button>
									</span>
								</div>
							</div> <!-- /.col -->
						</div>
					</div>
					<div class="body table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Tgl. Input</th>
									<th>Nama</th>
									<th>No. Telp</th>
									<th>Alamat</th>
									<th>Counter Kirim</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="results">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal Detail Produk -->
<div class="modal fade" id="modal_guest" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="title_modal_guest">Data Tamu</h4>
			</div> <!-- /.modal-header -->
			<div class="modal-body">
				<form id="form_guest">
					<input type="hidden" name="id_guest" id="id_guest">
					<div class="row">
						<div class="col-md-5">
							<label for="name_guest">Nama *</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text" class="form-control" name="name_guest" id="name_guest" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<label for="phone_guest">No. Telepon *</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text" class="form-control mobile-phone-number" name="phone_guest" id="phone_guest" placeholder="Ex: +62 000-0000-0000" required>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<label for="guest_from">Tamu Dari *</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control" name="guest_from" id="guest_from" required>
										<option value="z">Zein</option>
										<option value="r">Ratna</option>
										<option value="zr">Zein & Ratna</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<div class="form-line">
									<label for="address_guest">Alamat</label>
									<textarea class="form-control" name="address_guest" id="address_guest"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<button type="submit" class="btn btn-success waves-effect">Simpan</button>
							<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Batal</button>
						</div>
					</div>
				</form>
			</div> <!-- /.modal-body -->
		</div> <!-- /.modal-content -->
	</div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<script>
	$(document).ready(function() {
		$('.mobile-phone-number').inputmask('+62 999-9999-9999', {
			placeholder: '+62 ___-____-____'
		});

		listGuest();
	});

	$("#results").on("click", ".pagination a", function(e) {
		e.preventDefault();
		let page = $(this).attr("data-page");
		listGuest(page);
	});

	function listGuest(page) {
		if (page == undefined) {
			page = localStorage.getItem('page-guest');
			if (page == undefined) {
				page = 1;
			}
		} else {
			localStorage.setItem('page-guest', page);
		}

		let sortBy = getFilter('sortBy-guest', '#sort_by');
		let orderBy = getFilter('orderBy-guest', '#order_by');

		$('#loading').show();
		$("#results").html(`<tr><td class="text-center" colspan="7"><i>loading...</i></td></tr>`);
		$("#results").load("guest/data_table", {
			"page": page,
			"sort_by": (sortBy == "") ? "created_at" : sortBy,
			"order_by": (orderBy == "") ? "desc" : orderBy,
			"search_by": getFilter('searchBy-guest', '#search_by'),
			"keywords": getFilter('keywords-guest', '#keywords')
		}, function() {
			$('#loading').hide();
		});
	}

	function setFilter(element, reload) {
		var lsName = $(element).attr("data-filter");
		localStorage.setItem(lsName, element.value);
		if (reload) {
			listGuest(1);
		}
	}

	function getFilter(lsName, elName) {
		var lsVal = localStorage.getItem(lsName);
		if (lsVal == undefined) {
			lsVal = $(elName).val();
			localStorage.setItem(lsName, lsVal);
		} else {
			$(elName).val(lsVal);
		}
		return lsVal;
	}

	function addGuest() {
		$("#id_guest").val("");
		$("#title_modal_guest").text("Tambah Tamu Undangan");
		document.getElementById("form_guest").reset();
		resetValidation();
		$("#modal_guest").modal("show");
	}

	async function editGuest(idGuest) {
		$("#id_guest").val(idGuest);
		$("#title_modal_guest").text("Edit Tamu Undangan");
		resetValidation();
		try {
			const data = await getDataGuest(idGuest);
			$("#name_guest").val(data[0].name_guest);
			$("#phone_guest").val(data[0].phone_guest);
			$("#guest_from").val(data[0].guest_from);
			$("#address_guest").val(data[0].address_guest);
			$("#modal_guest").modal("show");
		} catch (error) {
			swal("Error", error, "error");
		}
	}

	function getDataGuest(idGuest) {
		return fetch(`guest/get_data_guest/${idGuest}`)
			.then(response => response.json())
			.catch(error => console.log(error));
	}

	function deleteGuest(idGuest) {
		swal({
				text: "Yakin akan menghapus data ini?",
				icon: "warning",
				buttons: true,
				dangerMode: true
			})
			.then((res) => {
				if (res) {
					$("#loading").show();
					fetch("guest/delete", {
							method: "POST",
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
							},
							body: `id_guest=${idGuest}`
						})
						.then(response => response.json())
						.then(data => {
							if (data.status) {
								swal("Success", data.message, "success").then(value => listGuest());
							} else {
								swal("", data.message, "error");
							}
						})
						.catch(error => {
							console.log(error);
							swal("Error", "Silahkan hubungi admin", "error");
						})
						.finally(() => {
							$("#loading").hide();
						});
				}
			})
	}

	function resetValidation() {
		$('#form_guest').validate().resetForm();
		$('#form_guest input').parents('.form-line').removeClass('error');
		$('#form_guest select').parents('.form-line').removeClass('error');
	}

	$(function() {
		$("#form_guest").validate({
			highlight: function(input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function(input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function(error, element) {
				$(element).parents('.form-group').append(error);
			},
			submitHandler: function(form, event) {
				event.preventDefault();

				let formData = new FormData(form);
				const idGuest = formData.get("id_guest")

				let url = "";
				if (idGuest == "") {
					url = `guest/insert`;
				} else {
					url = `guest/update`;
				}

				$("#loading").show();
				fetch(url, {
						method: 'POST',
						body: formData
					})
					.then(response => response.json())
					.then(data => {
						if (data.status) {
							swal("Success", data.message, "success").then(() => {
								listGuest(1);
								$("#modal_guest").modal("hide");
							});
						} else {
							swal("", data.message, "error");
						}
					})
					.catch(error => {
						console.log('Error:', error);
						swal("Error", "Silahkan hubungi admin", "error");
					})
					.finally(() => {
						$("#loading").hide();
					});
			}
		})
	})

	function refresh() {
		localStorage.setItem('sortBy-guest', "created_at");
		localStorage.setItem('orderBy-guest', "desc");
		localStorage.setItem('searchBy-guest', "name_guest");
		localStorage.setItem('keywords-guest', "");
		listGuest(1);
	}
</script>