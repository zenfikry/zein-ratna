<?php if ($row_number == 0) { ?>
	<tr>
		<td class="text-center" colspan="7">
			<i>Data tidak ditemukan...</i>
		</td>
	</tr>
<?php } else { ?>
	<?php foreach ($guests as $guest) { ?>
		<tr>
			<td data-title="#"><?php echo $guest['id_guest'] ?></td>
			<td data-title="Tgl. Input"><?php echo $guest['created_date'] ?></td>
			<td data-title="Nama"><?php echo $guest['name_guest'] ?></td>
			<td data-title="No. Telp"><?php echo $guest['phone_guest'] ?></td>
			<td data-title="Alamat"><?php echo $guest['address_guest'] ?></td>
			<td data-title="Counter Kirim"><?php echo $guest['sent_counter'] ?></td>
			<td data-title="Aksi" class="text-center">
				<a class="my-btn-action my-btn-edit" onclick="editGuest(<?php echo $guest['id_guest'] ?>)">
					<i class="material-icons">edit</i>
				</a>
				<a class="my-btn-action my-btn-delete" onclick="deleteGuest(<?php echo $guest['id_guest'] ?>)">
					<i class="material-icons">delete</i>
				</a>
			</td>
		</tr>
	<?php } ?>
	<tr>
		<td class="text-right" colspan="7" id="paging"><?php echo $paging ?></td>
	</tr>
<?php } ?>