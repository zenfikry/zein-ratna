<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Log In | Webadmin</title>
	<!-- Favicon-->
	<link rel="icon" href="<?php echo base_url() ?>assets-admin/favicon.ico" type="image/x-icon">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url() ?>assets-admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Waves Effect Css -->
	<link href="<?php echo base_url() ?>assets-admin/plugins/node-waves/waves.css" rel="stylesheet" />

	<!-- Animation Css -->
	<link href="<?php echo base_url() ?>assets-admin/plugins/animate-css/animate.css" rel="stylesheet" />

	<!-- Custom Css -->
	<link href="<?php echo base_url() ?>assets-admin/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
	<div class="login-box">
		<div class="logo">
			<a href="javascript:void(0);">Web<b>Admin</b></a>
			<small>Admin BootStrap Based - Material Design</small>
		</div>
		<div class="card">
			<div class="body">
				<form id="form_login">
					<div class="msg">Sign in to start your session</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">person</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" name="username" id="username_admin" placeholder="Username" required autofocus>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">lock</i>
						</span>
						<div class="form-line">
							<input type="password" class="form-control" name="password" id="password_admin" placeholder="Password" required>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-8 p-t-5">

						</div>
						<div class="col-xs-4">
							<button class="btn btn-block bg-pink waves-effect" type="submit">Login</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Jquery Core Js -->
	<script src="<?php echo base_url() ?>assets-admin/plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core Js -->
	<script src="<?php echo base_url() ?>assets-admin/plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Waves Effect Plugin Js -->
	<script src="<?php echo base_url() ?>assets-admin/plugins/node-waves/waves.js"></script>

	<!-- Validation Plugin Js -->
	<script src="<?php echo base_url() ?>assets-admin/plugins/jquery-validation/jquery.validate.js"></script>
	<script src="<?php echo base_url() ?>assets-admin/plugins/jquery-validation/localization/messages_id.js"></script>

	<!-- Sweetalert -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script>
		$(function() {
			$("#form_login").validate({
				highlight: function(input) {
					$(input).parents('.form-line').addClass('error');
				},
				unhighlight: function(input) {
					$(input).parents('.form-line').removeClass('error');
				},
				errorPlacement: function(error, element) {
					$(element).parents('.form-group').append(error);
				},
				submitHandler: function(form, event) {
					event.preventDefault();

					let formData = new FormData(form);

					$("#loading").show();
					fetch(`login/act_login`, {
							method: 'POST',
							body: formData
						})
						.then(response => response.json())
						.then(data => {
							if (data.status) {
								swal("Success", data.message, "success").then(() => {
									window.open('<?php echo base_url('webadmin') ?>', '_self');
								});
							} else {
								swal("", data.message, "error");
							}
						})
						.catch(error => {
							console.log('Error:', error);
							swal("Error", "Silahkan hubungi admin", "error");
						})
						.finally(() => {
							$("#loading").hide();
						});
				}
			})
		});
	</script>
</body>

</html>