<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wedding extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Reservation_model', 'Wedding_wishes_model', 'Guest_model'));
	}

	public function index($slug = null)
	{
		$name = "...";
		if ($slug != null) {
			$guest = $this->Guest_model->get_guest(array('slug_guest' => $slug));
			if (count($guest) > 0) {
				$name = $guest[0]['name_guest'];
			}
		}
		$data['name_guest'] = $name;
		$this->load->view('wedding/index', $data);
	}

	public function save_reservation()
	{
		$data = array(
			'name_reservation' => $this->input->post('name'),
			'address_reservation' => $this->input->post('address'),
			'attend_reservation' => $this->input->post('attend'),
			'created_at' => date("Y/m/d H:i:s")
		);

		$query = $this->Reservation_model->save_reservation($data);
		if ($query) {
			$json = array('status' => true, 'message' => 'Berhasil reservasi...');
		} else {
			$json = array('status' => false, 'message' => 'Gagal reservasi...');
		}

		echo json_encode($json, true);
	}

	public function save_wedding_wishes()
	{
		$data = array(
			'name_wedding_wishes' => $this->input->post('name'),
			'wedding_wishes' => $this->input->post('message'),
			'created_at' => date("Y/m/d H:i:s")
		);

		$query = $this->Wedding_wishes_model->save_wedding_wishes($data);
		if ($query) {
			$json = array('status' => true, 'message' => 'Berhasil kirim ucapan...');
		} else {
			$json = array('status' => false, 'message' => 'Gagal kirim ucapan...');
		}

		echo json_encode($json, true);
	}

	public function get_wedding_wishes()
	{
		$wedding_wishes = $this->Wedding_wishes_model->get_wedding_wishes(array('is_publish' => 1), 'created_at desc');
		$data = array();
		foreach ($wedding_wishes as $wish) {
			$row = array();
			$row['name'] = $wish['name_wedding_wishes'];
			$row['message'] = $wish['wedding_wishes'];
			$data[] = $row;
		}
		echo json_encode($data, true);
	}
}
