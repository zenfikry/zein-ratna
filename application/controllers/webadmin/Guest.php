<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guest extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Guest_model'));

		if (!$this->session->has_userdata('id_admin')) {
			redirect('webadmin/login');
		}
	}

	public function index()
	{
		$this->load->view("webadmin/header");
		$this->load->view("webadmin/guest/index");
		$this->load->view("webadmin/footer");
	}

	public function data_table()
	{
		if ($this->input->post('page')) {
			$page_number = $this->input->post('page');
		} else {
			$page_number = '1';
		}

		$sort_by = $this->input->post('sort_by');
		$order_by = $this->input->post('order_by');
		$search_by = $this->input->post('search_by');
		$keywords = $this->input->post('keywords');
		$per_page = 10;

		$row_number = $this->Guest_model->guest_num_rows($search_by, $keywords);
		$total_page = ceil($row_number / $per_page);
		$page_position = (($page_number - 1) * $per_page);

		$guests = $this->Guest_model->list_guest($page_position, $per_page, $sort_by, $order_by, $search_by, $keywords);
		$data_guests = array();
		foreach ($guests as $guest) {
			$row = array();
			$row['id_guest'] = $guest['id_guest'];
			$row['created_date'] = date("d-M-Y H:i", strtotime($guest['created_at']));
			$row['name_guest'] = $guest['name_guest'];
			$row['phone_guest'] = $guest['phone_guest'];
			$row['address_guest'] = $guest['address_guest'];
			$row['guest_from'] = $guest['guest_from'];
			$row['sent_counter'] = $guest['sent_counter'];
			$data_guests[] = $row;
		}

		$data['guests'] = $data_guests;
		$data['row_number'] = $row_number;
		$data['paging'] = $this->functionpaging->paging($per_page, $page_number, $row_number, $total_page);
		$this->load->view("webadmin/guest/data", $data);
	}

	public function get_data_guest($id)
	{
		$guests = $this->Guest_model->get_guest(array('id_guest' => $id));
		echo json_encode($guests, true);
	}

	public function insert()
	{
		$phone_guest = $this->input->post('phone_guest');
		$clean_phone_guest = preg_replace('/\s+|-/', '', $phone_guest);
		/* name validation */
		$validate_name = $this->Guest_model->get_guest(array('name_guest' => $this->input->post('name_guest')));
		if (count($validate_name) > 0) {
			echo json_encode(array('status' => false, 'message' => 'Nama : "' . $this->input->post('name_guest') . '" sudah ada'), true);
			return false;
		}

		/* phone validation */
		$validate_phone = $this->Guest_model->get_guest(array('phone_guest' => $clean_phone_guest));
		if (count($validate_phone) > 0) {
			echo json_encode(array('status' => false, 'message' => 'No. Telepon : "' . $phone_guest . '" sudah digunakan'), true);
			return false;
		}

		$data = array(
			'name_guest' => $this->input->post('name_guest'),
			'phone_guest' => $clean_phone_guest,
			'guest_from' => $this->input->post('guest_from'),
			'address_guest' => $this->input->post('address_guest'),
			'slug_guest' => url_title($this->input->post('name_guest'), 'dash', true),
			'created_at' => date("Y/m/d H:i:s"),
			'created_by' => $this->session->userdata('id_admin')
		);

		$query = $this->Guest_model->save_guest($data);
		if ($query) {
			$json = array('status' => true, 'message' => 'Tamu undangan berhasil disimpan');
		} else {
			$json = array('status' => false, 'message' => 'Tamu undangan gagal disimpan');
		}

		echo json_encode($json, true);
	}

	public function update()
	{
		$id_guest = $this->input->post('id_guest');
		$phone_guest = $this->input->post('phone_guest');
		$clean_phone_guest = preg_replace('/\s+|-/', '', $phone_guest);
		/* name validation */
		$validate_name = $this->Guest_model->get_guest(array('name_guest' => $this->input->post('name_guest'), 'id_guest !=' => $id_guest));
		if (count($validate_name) > 0) {
			echo json_encode(array('status' => false, 'message' => 'Nama : "' . $this->input->post('name_guest') . '" sudah ada'), true);
			return false;
		}

		/* phone validation */
		$validate_phone = $this->Guest_model->get_guest(array('phone_guest' => $clean_phone_guest, 'id_guest !=' => $id_guest));
		if (count($validate_phone) > 0) {
			echo json_encode(array('status' => false, 'message' => 'No. Telepon : "' . $phone_guest . '" sudah digunakan'), true);
			return false;
		}

		$data = array(
			'name_guest' => $this->input->post('name_guest'),
			'phone_guest' => $clean_phone_guest,
			'guest_from' => $this->input->post('guest_from'),
			'address_guest' => $this->input->post('address_guest'),
			'slug_guest' => url_title($this->input->post('name_guest'), 'dash', true),
			'modified_at' => date("Y/m/d H:i:s"),
			'modified_by' => $this->session->userdata('id_admin')
		);

		$query = $this->Guest_model->update_guest($data, array('id_guest' => $id_guest));
		if ($query) {
			$json = array('status' => true, 'message' => 'Tamu undangan berhasil disimpan');
		} else {
			$json = array('status' => false, 'message' => 'Tamu undangan gagal disimpan');
		}

		echo json_encode($json, true);
	}

	public function delete()
	{
		$id_guest = $this->input->post('id_guest');
		$query = $this->Guest_model->delete_guest(array('id_guest' => $id_guest));
		if ($query) {
			$json = array('status' => true, 'message' => 'Tamu undangan berhasil dihapus');
		} else {
			$json = array('status' => false, 'message' => 'Tamu undangan gagal dihapus');
		}
		
		echo json_encode($json, true);
	}
}
