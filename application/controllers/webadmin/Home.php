<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->model(array('Admin_model'));

		if (!$this->session->has_userdata('id_admin')) {
			redirect('webadmin/login');
		}
	}

	public function index()
	{
		$this->load->view("webadmin/header");
		$this->load->view("webadmin/home");
		$this->load->view("webadmin/footer");
	}
}
