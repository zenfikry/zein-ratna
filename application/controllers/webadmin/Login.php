<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Admin_model'));
	}

	public function index()
	{
		$this->load->view("webadmin/login");
	}

	public function act_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$where = array('username_admin' => $username, 'password_admin' => md5($password));
		$admin = $this->Admin_model->get_admin($where);
		if (count($admin) == 1) {
			$this->session->set_userdata('id_admin', $admin[0]['id_admin']);
			$this->session->set_userdata('name_admin', $admin[0]['name_admin']);
			$this->session->set_userdata('username_admin', $admin[0]['username_admin']);

			$json = array('status' => true, 'message' => 'Login berhasil...');
		} else {
			$json = array('status' => false, 'message' => 'Login gagal. Username atau password anda salah.');
		}

		echo json_encode($json, true);
	}

	public function act_logout()
	{
		$data = array('id_admin', 'name_admin', 'username_admin');
		$this->session->unset_userdata($data);
		redirect('webadmin/');
	}
}
