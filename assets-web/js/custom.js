/* version 1.0.0 */
const baseUrl = $("#base_url").val();

$("#form_reservation").submit(function(event) {
	event.preventDefault();
	const formData = new FormData(this);
	const message = `Halo Zein & Ratna, saya *${formData.get('name')}* dari *${formData.get('address')}* ingin konfirmasi kehadiran undangan pernikahan pada tanggal 13 Juni 2021 bahwa *${formData.get('attend')}* Terimakasih.`;

	fetch(`${baseUrl}wedding/save_reservation`, {
		method: 'POST',
		body: formData
	})
	.then(response => response.json())
	.then(data => {
		console.log(`Reservation: ${data.status}`);
	})
	.catch(error => {
		console.log('Error:', error);
	})
	.finally(() => {
		document.getElementById("form_reservation").reset();
		sendWhatsapp('6285324475915', escapeHtml(message))
	});	
})

$("#form_wedding_wishes").submit(function(event) {
	event.preventDefault();
	const formData = new FormData(this);
	
	$("#btn_send_wishes").text("loading...");
	$("#btn_send_wishes").attr("disabled", true);
	fetch(`${baseUrl}wedding/save_wedding_wishes`, {
		method: 'POST',
		body: formData
	})
	.then(response => response.json())
	.then(data => {
		if (data.status) {
			document.getElementById("form_wedding_wishes").reset();
			loadWeddingWishes();
		} else {
			swal("", data.message, "error");
		}
	})
	.catch(error => {
		console.log('Error:', error);
	})
	.finally(() => {
		$("#btn_send_wishes").text("Kirim");
		$("#btn_send_wishes").attr("disabled", false);
	});
})

function escapeHtml(text) {
	const specialChar = {
		'&': '%26',
		'"': '%22',
		"'": '%27',
		',': '%2C'
	};
	
	return text.replace(/[&<>"']/g, function(m) { return specialChar[m]; });
}

function loadWeddingWishes() {
	fetch(`wedding/get_wedding_wishes`)
	.then(response => response.json())
	.then(results => {
		let layout = '';
		results.forEach(data => {
			layout += layoutWeddingWishes(data);
		});
		$("#list_wedding_wishes").html(layout);
	})
	.catch(error => {
		console.log('Error:', error);
	})
	.finally(() => {
		
	});
}

function layoutWeddingWishes(data) {
	return `<div class="row">
				<div class="product_item_line name-item col-md-10 col-md-push-1 col-sm-8 col-sm-push-2 col-xs-12">
					<a class="gla_product_thumbnail">
						<img alt="" src="${baseUrl}assets-web/images/ring.png" style="width: 50px;">
					</a>
					<div class="product-info text-justify">
						<a>${data.name}</a>
						<p class="text-justify" style="max-width: none; margin-bottom: 0px;">${data.message}</p>
					</div>
				</div>
			</div>`
}

function sendWhatsapp(phoneNumber, message) {
	let text = "";
	if (message != undefined && message != "") {
		text = `&text=${message}`;
	}

	if (phoneNumber.substr(0, 1) == "0") {
		window.open(`http://api.whatsapp.com/send?phone=62${phoneNumber.substr(1)}${text}`, '_blank');
	} else {
		if (phoneNumber.substr(0, 2) == "62") {
			window.open(`http://api.whatsapp.com/send?phone=${phoneNumber}${text}`, '_blank');
		} else if (phoneNumber.substr(0, 3) == "+62") {
			window.open(`http://api.whatsapp.com/send?phone=${phoneNumber}${text}`, '_blank');
		} else {
			bootbox.alert("Nomor HP tidak valid");
		}
	}
}